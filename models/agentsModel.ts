import * as mySql from "mysql";
import { DataBaseConfig } from "../globals/config/database";

export class Agents {
    static pool: mySql.Pool;
    static initialized = true;
    static async createConnection(): Promise<mySql.Pool> {
        try {
            this.pool = await DataBaseConfig.connect();
            return this.pool;
        } catch (error) {
            console.log('error in Initializing Agent Conversations Model');
            throw new Error(error);
        }
        // Database Connection For Visitors Based Operation on Visitor Collections

    }
   
    //CRUD Operations
    public static async InsertAgent() {
        //create connection
        await this.createConnection();
        this.pool.query('');
    }
}