
/* Author : Saad Ismail Shaikh
   Date   : 18-1-2015
   WARNING : Please Don Not Edit File Unless You Know Application Flow.
*/
/* ---------------------------------------------- Note (Important) ------------------------------------------
// |       --MiddleWare Sit on top of Controller Acts as Partial Controller.                                 |
// |      -- A Request Will be denied controlled from Here.                                                   |
// |      -- With The Help of Body Parsing This Layer will Decide on                                          |
// |      -- which Route/Controller a Request should be directed.                                             |
// -----------------------------------------------------------------------------------------------------------
// Express is used to Create Node Server and handling Routes (Controller Logic of MVC is implemented here)
*/
import * as express from "express";


// Body Parser is used to parse Request Url body
import * as bodyParser from "body-parser";

// import Common Routes
import * as Routes from "./config/routes";
import * as mySql from "mysql";

import * as Constants from "./config/constants"

import { DataBaseConfig } from "./config/database";

import { Agents } from "../models/agentsModel";

// child.on('message', (data) => { console.log('data recieved'); })



const cors = require('cors');
const compression = require('compression')


class __biZZCMiddleWare {
    //Static Initialization

    private static Instance: __biZZCMiddleWare;
    public ConcurrentChatLimit = 20;

    private application;

    private constructor() {
        this.application = express();
        //Set Port
        this.application.set('port', Constants.port);
    }

    public static GetInstance(): __biZZCMiddleWare {
        if (!__biZZCMiddleWare.Instance) {
            __biZZCMiddleWare.Instance = new __biZZCMiddleWare();
            return __biZZCMiddleWare.Instance
        } else {
            return __biZZCMiddleWare.Instance;
        }
    }

    public async InitApplication(): Promise<any> {
        try {


            console.log('Environment: ' + process.env.NODE_ENV);
            console.log('DB Address: ' + process.env.DB_ADDRESS);
            // console.log((process.env.NODE_ENV == 'production') ? (process.env.DB_ADDRESS) ? process.env.DB_ADDRESS : 'not Db Address' : '!production');

            //Creating Single Database Connection And Pooling Connection to Rest of The Application;
            console.log('Request Handler Connecting Database First Instance');
            await DataBaseConfig.connect((process.env.NODE_ENV == 'production') ? (process.env.DB_ADDRESS) ? process.env.DB_ADDRESS : 'localhost' : undefined).then(result => {
                this.RegisterMiddleWare();
            });
            /* 
            ====================== NOTE =========================
            //All The Collection Should Go In Following Function To Keep The Singleton Nature of Database Object.
            //To Effectively Use Connection Pooling of MongoDb Driver. 
            =====================================================
            */
            //    console.log('Database!');
            
            // dbase.getConnection((err, connection) => {
            //     if(err){
            //         console.log('Error connecting to database...');
            //         console.log(err);
            //     }
            // })
            //     console.log(dbase);
            // if (dbase.config.database) { 
            //     console.log(dbase);
            //     console.log('Database Connected!');
            // } else {
            //     throw new Error('Error connecting database')
            // }
            // await this.InitCollections();
            //console.log('After init Collections');



            /* 
            ====================== NOTE =========================
            //All The Rest Communication Should Go In Following Function
            //For Nested Routes for example /api/method follow the steps below.
            1. First Add Routes in Controller along with their method Type
            2. Grab Your Router as Export member in '/config/routes'
            3. Add Your Api Router and use Imported Router Object Accordingly. 
            =====================================================
            */
            
            //console.log('After RegisterMiddleWare');

            return this.application;

        } catch (error) {
            //console.log(error);
            console.log('Error in Initializing Application');
            //server.close();
            throw new Error(error);
        }
    }


    private RegisterMiddleWare() {

        this.application.use((req, res, next) => {
            if (req.get('x-amz-sns-message-type')) {
                req.headers['content-type'] = 'application/json';
            }
            next();
        });

        // Parse url query string as json
        this.application.use(bodyParser.json());
        //application.use(cors());

        // Extended property allows to have embedded object in query string URI.
        // See Following Reference
        //https://stackoverflow.com/questions/29960764/what-does-extended-mean-in-express-4-0
        this.application.use(bodyParser.urlencoded({
            extended: true
        }));

        // Middle Ware Handler To hand Cross Origin Angular Requests;
        //In Both The Cases They Request Files but they Don't need To Generate Session On Fetch
        this.application.use((req, res, next) => {
            if (req.method === 'OPTIONS') {
                if (req.headers.origin) {
                    res.header("Access-Control-Allow-Origin", (req.headers.origin as string));
                    res.header("Access-Control-Allow-Headers", "content-type");
                    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
                    res.header('Access-Control-Allow-Credentials', 'true');
                    res.header('Connection', 'keep-alive');
                    res.header('Content-Length', '0');
                    res.header('Date', new Date().toISOString());
                    res.header('Vary', 'Origin, Access-Control-Request-Headers');
                }
                res.status(204);
                res.end();
            }
            else {
                //       console.log('Called Server');
                next();
            }
        });

        // if ((process.env.NODE_ENV != 'production')) {
        this.application.use(compression());
        this.application.use(cors({ credentials: true, origin: ['http://localhost:8001', 'http://localhost:4200', 'http://localhost:4201', 'http://192.168.20.73:4200', 'https://admin.beelinks.solutions', 'https://reseller.beelinks.solutions', 'http://192.168.20.100:4200', 'http://192.168.20.6:4200',,'http://localhost:8001','https://ca741ca3.ngrok.io','https://b525abd8.ngrok.io','https://5bc2637a.ngrok.io'] }));
        // }

        this.application.use('/', Routes.StaticRoutes);

        this.application.get('*', function (req, res) {
            res.status(401).send('what???');
        });


    }
    public get Application() {
        return this.application;
    }
}

export const __biZZC_Core = __biZZCMiddleWare.GetInstance();

Object.seal(__biZZCMiddleWare);