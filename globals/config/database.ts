

//Created By Saad Ismail Shaikh
//Date : 19-1-2018

//Note : Its Main Database Config File Which Later can be used
// to configure production and Development Environment.

//MongoDB Client Object from Node_modules MongoDB.
// import { MongoClient, Db } from "mongodb";
import * as mySql from "mysql";
import { __biZZC_Core } from "../__biZZCMiddleWare";


// For Development Environment 
// Host : localhost or 127.0.0.1
// Port : 27017 which is Default port of MongoDB;
let devURI = 'localhost:27017';
// let dbName = "local";

// let devURI = 'mongodb://192.168.20.92:27017/';
let dbName = "local";

// Singleton Class
// Global database Class which will be used throughout the api of the application.
export class DataBaseConfig {
    private dataBase: any;

    // Single Object instance of this class
    private static Instance: DataBaseConfig;

    // Contructor is private  means the object can't be initialized directly.
    private constructor(prodURI?: string) {
        this.dataBase = undefined;
    }

    // Connect initialize database connection upon application start in index.js
    public static async connect(prodURI?: string): Promise<mySql.Pool> {
        if (!prodURI) {
            prodURI = devURI;
        }

        try {
            if (!DataBaseConfig.Instance) {
                DataBaseConfig.Instance = new DataBaseConfig();
                let sqlClient = await DataBaseConfig.Instance.connectDatabase(prodURI);

                DataBaseConfig.Instance.dataBase = sqlClient;
                //console.log("Database Name : " + DataBaseConfig.Instance.dataBase.databaseName);
                return DataBaseConfig.Instance.dataBase;
            } else return DataBaseConfig.Instance.dataBase

        } catch (error) {
            console.log(error);
            console.log('error in Connecting To Database');
            console.log(process.env.NODE_ENV);
            console.log(process.env.DB_ADDRESS);
            // await DataBaseConfig.Instance.connectDatabase(prodURI);
            // DataBaseConfig.Instance = undefined;
            throw new Error(error);
        }
    }

    public async connectDatabase(prodURI) : Promise<mySql.Pool>{
        try {
            return new Promise((resolve,reject) => {

                console.log('Connecting to Database...');
    
                let pool = mySql.createPool({
                    connectionLimit : 10,
                    host            : 'example.org',
                    user            : 'bob',
                    password        : 'secret',
                    database        : 'my_db'
                });
                pool.getConnection(async (err, connection) => {
                    if(err){
                        console.log('error in connecting to database...')
                        console.log('retrying...')
                        await DataBaseConfig.Instance.connectDatabase(prodURI);
                        // throw new Error('error in connecting to database...');
                        reject(err);
                        // await DataBaseConfig.Instance.connectDatabase(prodURI)
                        // return;
                    }
                    resolve(pool);
                })
            })
            // client.db(dbName);
            // let database = client.db(dbName);
        } catch (error) {
            // console.log('error in connecting Database');
            // let client = await DataBaseConfig.Instance.connectDatabase(prodURI)
            // return client;
            throw new Error('error in connecting to database...')
        }
    }


    // In Case if you want to connect to another database in between application 
    // First call disconnect and then connect to ur URI
    public disconnect(): void {
        if (DataBaseConfig.Instance && DataBaseConfig.Instance.dataBase) {
            DataBaseConfig.Instance.dataBase.destroy();
        }
        else {
            //console.log('No Database Initialized');
        }
    }
}
