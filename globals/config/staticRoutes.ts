
//Created By Saad Ismail Shaikh
//Date : 19-1-2018


//Express Module Reference
import * as express from 'express';
// Path Object to Define "Default/Static/Generic" Routes
import * as path from "path";

//import * as request from 'request-promise';
//import { Headers } from 'request';
//const requestIp = require('request-ip');


const { URL } = require('url');

// Main Entry Point of our app or Home Route for our app that will be delivered on default routes (Our Single Page Application)
// Angular DIST output folder
// ../        (ROOT)
//  |---->../build/dist/index.html (Output of Angular app/src)
// Since this will contain our static assest hence this path will remain static.

//Router Object Which Will be used to validate requests in Request Handler.
var router: express.Router = express.Router();

const publicPath = path.resolve(__dirname + '/../../');

const AngularRoutes = [
    '/login',
    '/home'
]



//Default Home Pages Routes of Application.
router.get('/', (req, res) => {

    res.sendFile(publicPath + '/public/dist/index.html');

});


router.get(AngularRoutes, (req, res) => {
    res.sendFile(publicPath + '/public/dist/index.html');
});


router.get('/js/*', (req, res) => {
    res.sendFile(publicPath + '/public/dist/' + path.basename(req.path));
    // console.log('caught');
});


//NEW ROUTES ADDED TO OPTIMIZE BUILD
router.get('/css/*', (req, res) => {
    res.sendFile(publicPath + '/public/dist/' + path.basename(req.path));
});


router.get('/assets/ccss/*', (req, res) => {
    res.sendFile(publicPath + '/public/static' + path.dirname(req.path) + "/" + path.basename(req.path));
});

router.get('/assets/packages/*', (req, res) => {
    //console.log(publicPath + '/public/static' + path.dirname(req.path) +  "/" + path.basename(req.path));
    res.sendFile(publicPath + '/public/static' + path.dirname(req.path) + "/" + path.basename(req.path));
});

router.get('/assets/img/backgrounds/*', (req, res) => {
    //console.log(publicPath + '/public/dist/assets/img/backgrounds/' + path.basename(req.path));
    res.sendFile(publicPath + '/public/dist/assets/img/backgrounds/' + path.basename(req.path));
});





router.get('/assets/img/icons/*', (req, res) => {
    //console.log(publicPath + '/public/dist/assets/img/icons/' + path.basename(req.path));
    res.sendFile(publicPath + '/public/dist/assets/img/icons/' + path.basename(req.path));
});

router.get('/assets/img/*', (req, res) => {
    // console.log(publicPath + '/public/dist/assets/img/' + path.basename(req.path));
    res.sendFile(publicPath + '/public/dist/assets/img/' + path.basename(req.path));
});

router.get('/img/*', (req, res) => {
    // console.log(publicPath + '/public/dist/assets/' + req.path);
    res.sendFile(publicPath + '/public/dist/assets/' + req.path);
});

router.get('/images/*', (req, res) => {
    res.sendFile(publicPath + '/public/static/assets/images/' + path.basename(req.path));
});

router.get('/fonts/*', (req, res) => {
    res.sendFile(publicPath + '/public/static/assets/fonts/' + path.basename(req.path));
});


export const StaticRoutes: express.Router = router;