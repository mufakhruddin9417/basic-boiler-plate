"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
/* Author : Saad Ismail Shaikh
   Date   : 18-1-2015
   WARNING : Please Don Not Edit File Unless You Know Application Flow.
*/
/* ---------------------------------------------- Note (Important) ------------------------------------------
// |       --MiddleWare Sit on top of Controller Acts as Partial Controller.                                 |
// |      -- A Request Will be denied controlled from Here.                                                   |
// |      -- With The Help of Body Parsing This Layer will Decide on                                          |
// |      -- which Route/Controller a Request should be directed.                                             |
// -----------------------------------------------------------------------------------------------------------
// Express is used to Create Node Server and handling Routes (Controller Logic of MVC is implemented here)
*/
var express = require("express");
// Body Parser is used to parse Request Url body
var bodyParser = require("body-parser");
// import Common Routes
var Routes = require("./config/routes");
var Constants = require("./config/constants");
var database_1 = require("./config/database");
// child.on('message', (data) => { console.log('data recieved'); })
var cors = require('cors');
var compression = require('compression');
var __biZZCMiddleWare = /** @class */ (function () {
    function __biZZCMiddleWare() {
        this.ConcurrentChatLimit = 20;
        this.application = express();
        //Set Port
        this.application.set('port', Constants.port);
    }
    __biZZCMiddleWare.GetInstance = function () {
        if (!__biZZCMiddleWare.Instance) {
            __biZZCMiddleWare.Instance = new __biZZCMiddleWare();
            return __biZZCMiddleWare.Instance;
        }
        else {
            return __biZZCMiddleWare.Instance;
        }
    };
    __biZZCMiddleWare.prototype.InitApplication = function () {
        return __awaiter(this, void 0, void 0, function () {
            var error_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        console.log('Environment: ' + process.env.NODE_ENV);
                        console.log('DB Address: ' + process.env.DB_ADDRESS);
                        // console.log((process.env.NODE_ENV == 'production') ? (process.env.DB_ADDRESS) ? process.env.DB_ADDRESS : 'not Db Address' : '!production');
                        //Creating Single Database Connection And Pooling Connection to Rest of The Application;
                        console.log('Request Handler Connecting Database First Instance');
                        return [4 /*yield*/, database_1.DataBaseConfig.connect((process.env.NODE_ENV == 'production') ? (process.env.DB_ADDRESS) ? process.env.DB_ADDRESS : 'localhost' : undefined).then(function (result) {
                                _this.RegisterMiddleWare();
                            })];
                    case 1:
                        _a.sent();
                        /*
                        ====================== NOTE =========================
                        //All The Collection Should Go In Following Function To Keep The Singleton Nature of Database Object.
                        //To Effectively Use Connection Pooling of MongoDb Driver.
                        =====================================================
                        */
                        //    console.log('Database!');
                        // dbase.getConnection((err, connection) => {
                        //     if(err){
                        //         console.log('Error connecting to database...');
                        //         console.log(err);
                        //     }
                        // })
                        //     console.log(dbase);
                        // if (dbase.config.database) { 
                        //     console.log(dbase);
                        //     console.log('Database Connected!');
                        // } else {
                        //     throw new Error('Error connecting database')
                        // }
                        // await this.InitCollections();
                        //console.log('After init Collections');
                        /*
                        ====================== NOTE =========================
                        //All The Rest Communication Should Go In Following Function
                        //For Nested Routes for example /api/method follow the steps below.
                        1. First Add Routes in Controller along with their method Type
                        2. Grab Your Router as Export member in '/config/routes'
                        3. Add Your Api Router and use Imported Router Object Accordingly.
                        =====================================================
                        */
                        //console.log('After RegisterMiddleWare');
                        return [2 /*return*/, this.application];
                    case 2:
                        error_1 = _a.sent();
                        //console.log(error);
                        console.log('Error in Initializing Application');
                        //server.close();
                        throw new Error(error_1);
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    __biZZCMiddleWare.prototype.RegisterMiddleWare = function () {
        this.application.use(function (req, res, next) {
            if (req.get('x-amz-sns-message-type')) {
                req.headers['content-type'] = 'application/json';
            }
            next();
        });
        // Parse url query string as json
        this.application.use(bodyParser.json());
        //application.use(cors());
        // Extended property allows to have embedded object in query string URI.
        // See Following Reference
        //https://stackoverflow.com/questions/29960764/what-does-extended-mean-in-express-4-0
        this.application.use(bodyParser.urlencoded({
            extended: true
        }));
        // Middle Ware Handler To hand Cross Origin Angular Requests;
        //In Both The Cases They Request Files but they Don't need To Generate Session On Fetch
        this.application.use(function (req, res, next) {
            if (req.method === 'OPTIONS') {
                if (req.headers.origin) {
                    res.header("Access-Control-Allow-Origin", req.headers.origin);
                    res.header("Access-Control-Allow-Headers", "content-type");
                    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
                    res.header('Access-Control-Allow-Credentials', 'true');
                    res.header('Connection', 'keep-alive');
                    res.header('Content-Length', '0');
                    res.header('Date', new Date().toISOString());
                    res.header('Vary', 'Origin, Access-Control-Request-Headers');
                }
                res.status(204);
                res.end();
            }
            else {
                //       console.log('Called Server');
                next();
            }
        });
        // if ((process.env.NODE_ENV != 'production')) {
        this.application.use(compression());
        this.application.use(cors({ credentials: true, origin: ['http://localhost:8001', 'http://localhost:4200', 'http://localhost:4201', 'http://192.168.20.73:4200', 'https://admin.beelinks.solutions', 'https://reseller.beelinks.solutions', 'http://192.168.20.100:4200', 'http://192.168.20.6:4200', , 'http://localhost:8001', 'https://ca741ca3.ngrok.io', 'https://b525abd8.ngrok.io', 'https://5bc2637a.ngrok.io'] }));
        // }
        this.application.use('/', Routes.StaticRoutes);
        this.application.get('*', function (req, res) {
            res.status(401).send('what???');
        });
    };
    Object.defineProperty(__biZZCMiddleWare.prototype, "Application", {
        get: function () {
            return this.application;
        },
        enumerable: true,
        configurable: true
    });
    return __biZZCMiddleWare;
}());
exports.__biZZC_Core = __biZZCMiddleWare.GetInstance();
Object.seal(__biZZCMiddleWare);
//# sourceMappingURL=__biZZCMiddleWare.js.map