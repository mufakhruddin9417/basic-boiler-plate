"use strict";
//Created By Saad Ismail Shaikh
//Date : 19-1-2018
Object.defineProperty(exports, "__esModule", { value: true });
//Express Module Reference
var express = require("express");
// Path Object to Define "Default/Static/Generic" Routes
var path = require("path");
//import * as request from 'request-promise';
//import { Headers } from 'request';
//const requestIp = require('request-ip');
var URL = require('url').URL;
// Main Entry Point of our app or Home Route for our app that will be delivered on default routes (Our Single Page Application)
// Angular DIST output folder
// ../        (ROOT)
//  |---->../build/dist/index.html (Output of Angular app/src)
// Since this will contain our static assest hence this path will remain static.
//Router Object Which Will be used to validate requests in Request Handler.
var router = express.Router();
var publicPath = path.resolve(__dirname + '/../../');
var AngularRoutes = [
    '/login',
    '/home'
];
//Default Home Pages Routes of Application.
router.get('/', function (req, res) {
    res.sendFile(publicPath + '/public/dist/index.html');
});
router.get(AngularRoutes, function (req, res) {
    res.sendFile(publicPath + '/public/dist/index.html');
});
router.get('/js/*', function (req, res) {
    res.sendFile(publicPath + '/public/dist/' + path.basename(req.path));
    // console.log('caught');
});
//NEW ROUTES ADDED TO OPTIMIZE BUILD
router.get('/css/*', function (req, res) {
    res.sendFile(publicPath + '/public/dist/' + path.basename(req.path));
});
router.get('/assets/ccss/*', function (req, res) {
    res.sendFile(publicPath + '/public/static' + path.dirname(req.path) + "/" + path.basename(req.path));
});
router.get('/assets/packages/*', function (req, res) {
    //console.log(publicPath + '/public/static' + path.dirname(req.path) +  "/" + path.basename(req.path));
    res.sendFile(publicPath + '/public/static' + path.dirname(req.path) + "/" + path.basename(req.path));
});
router.get('/assets/img/backgrounds/*', function (req, res) {
    //console.log(publicPath + '/public/dist/assets/img/backgrounds/' + path.basename(req.path));
    res.sendFile(publicPath + '/public/dist/assets/img/backgrounds/' + path.basename(req.path));
});
router.get('/assets/img/icons/*', function (req, res) {
    //console.log(publicPath + '/public/dist/assets/img/icons/' + path.basename(req.path));
    res.sendFile(publicPath + '/public/dist/assets/img/icons/' + path.basename(req.path));
});
router.get('/assets/img/*', function (req, res) {
    // console.log(publicPath + '/public/dist/assets/img/' + path.basename(req.path));
    res.sendFile(publicPath + '/public/dist/assets/img/' + path.basename(req.path));
});
router.get('/img/*', function (req, res) {
    // console.log(publicPath + '/public/dist/assets/' + req.path);
    res.sendFile(publicPath + '/public/dist/assets/' + req.path);
});
router.get('/images/*', function (req, res) {
    res.sendFile(publicPath + '/public/static/assets/images/' + path.basename(req.path));
});
router.get('/fonts/*', function (req, res) {
    res.sendFile(publicPath + '/public/static/assets/fonts/' + path.basename(req.path));
});
exports.StaticRoutes = router;
//# sourceMappingURL=staticRoutes.js.map