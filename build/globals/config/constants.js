"use strict";
//Created By Saad Ismail Shaikh
//Date : 19-1-2018
Object.defineProperty(exports, "__esModule", { value: true });
// Note : This Module Contains Global settings of Application
// The Settings which are Stated Here will remain constant througout the application
exports.port = process.env.PORT || '8006';
//# sourceMappingURL=constants.js.map