"use strict";
//Created By Saad Ismail Shaikh
//Date : 19-1-2018
Object.defineProperty(exports, "__esModule", { value: true });
//Register All static or Dynamic Routes Here as export Object
//Static Routes Object "Global Routes"
var staticRoutes_1 = require("./staticRoutes");
exports.StaticRoutes = staticRoutes_1.StaticRoutes;
var admin_1 = require("../../controllers/admin");
exports.adminRoutes = admin_1.adminRoutes;
//# sourceMappingURL=routes.js.map