"use strict";
//Created By Saad Ismail Shaikh
//Date : 19-1-2018
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
//Note : Its Main Database Config File Which Later can be used
// to configure production and Development Environment.
//MongoDB Client Object from Node_modules MongoDB.
// import { MongoClient, Db } from "mongodb";
var mySql = require("mysql");
// For Development Environment 
// Host : localhost or 127.0.0.1
// Port : 27017 which is Default port of MongoDB;
var devURI = 'localhost:27017';
// let dbName = "local";
// let devURI = 'mongodb://192.168.20.92:27017/';
var dbName = "local";
// Singleton Class
// Global database Class which will be used throughout the api of the application.
var DataBaseConfig = /** @class */ (function () {
    // Contructor is private  means the object can't be initialized directly.
    function DataBaseConfig(prodURI) {
        this.dataBase = undefined;
    }
    // Connect initialize database connection upon application start in index.js
    DataBaseConfig.connect = function (prodURI) {
        return __awaiter(this, void 0, void 0, function () {
            var sqlClient, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!prodURI) {
                            prodURI = devURI;
                        }
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 5, , 6]);
                        if (!!DataBaseConfig.Instance) return [3 /*break*/, 3];
                        DataBaseConfig.Instance = new DataBaseConfig();
                        return [4 /*yield*/, DataBaseConfig.Instance.connectDatabase(prodURI)];
                    case 2:
                        sqlClient = _a.sent();
                        DataBaseConfig.Instance.dataBase = sqlClient;
                        //console.log("Database Name : " + DataBaseConfig.Instance.dataBase.databaseName);
                        return [2 /*return*/, DataBaseConfig.Instance.dataBase];
                    case 3: return [2 /*return*/, DataBaseConfig.Instance.dataBase];
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        error_1 = _a.sent();
                        console.log(error_1);
                        console.log('error in Connecting To Database');
                        console.log(process.env.NODE_ENV);
                        console.log(process.env.DB_ADDRESS);
                        // await DataBaseConfig.Instance.connectDatabase(prodURI);
                        // DataBaseConfig.Instance = undefined;
                        throw new Error(error_1);
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    DataBaseConfig.prototype.connectDatabase = function (prodURI) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                try {
                    return [2 /*return*/, new Promise(function (resolve, reject) {
                            console.log('Connecting to Database...');
                            var pool = mySql.createPool({
                                connectionLimit: 10,
                                host: 'example.org',
                                user: 'bob',
                                password: 'secret',
                                database: 'my_db'
                            });
                            pool.getConnection(function (err, connection) { return __awaiter(_this, void 0, void 0, function () {
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!err) return [3 /*break*/, 2];
                                            console.log('error in connecting to database...');
                                            console.log('retrying...');
                                            return [4 /*yield*/, DataBaseConfig.Instance.connectDatabase(prodURI)];
                                        case 1:
                                            _a.sent();
                                            // throw new Error('error in connecting to database...');
                                            reject(err);
                                            _a.label = 2;
                                        case 2:
                                            resolve(pool);
                                            return [2 /*return*/];
                                    }
                                });
                            }); });
                        })
                        // client.db(dbName);
                        // let database = client.db(dbName);
                    ];
                    // client.db(dbName);
                    // let database = client.db(dbName);
                }
                catch (error) {
                    // console.log('error in connecting Database');
                    // let client = await DataBaseConfig.Instance.connectDatabase(prodURI)
                    // return client;
                    throw new Error('error in connecting to database...');
                }
                return [2 /*return*/];
            });
        });
    };
    // In Case if you want to connect to another database in between application 
    // First call disconnect and then connect to ur URI
    DataBaseConfig.prototype.disconnect = function () {
        if (DataBaseConfig.Instance && DataBaseConfig.Instance.dataBase) {
            DataBaseConfig.Instance.dataBase.destroy();
        }
        else {
            //console.log('No Database Initialized');
        }
    };
    return DataBaseConfig;
}());
exports.DataBaseConfig = DataBaseConfig;
//# sourceMappingURL=database.js.map